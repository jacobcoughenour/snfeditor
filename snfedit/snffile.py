from .fileparser import FileParser


class SnfFile(object):

	def __init__(self, path):
		self._data = None
		self.path = path

	@property
	def data(self):

		if self._data is None:
			self.load()

		return self._data

	# @data.setter
	# def data(self, value):
	# 	print("setter")
	# 	self._data = value

	def load(self):

		with open(self.path, "r") as rawfile:
			parser = FileParser()
			parser.feed(rawfile.read())
			self._data = parser.snValues
			rawfile.close()

	def save(self):
		print("save")
