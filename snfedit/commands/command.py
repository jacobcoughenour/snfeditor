
def command(func):
    def wrapper():
        func()
    return wrapper
f