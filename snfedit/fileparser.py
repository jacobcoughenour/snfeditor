from html.parser import HTMLParser


class FileParser(HTMLParser):

	def __init__(self, *args):
		HTMLParser.__init__(self, *args)
		# current list of Starry Night values
		self.snValues = {}

	def handle_starttag(self, tag, attrs):

		# only SN_VALUE tags
		if tag == "sn_value":

			keys = attrs[0][1].split('_')
			values = attrs[1][1].split(', ')

			self.setValues(self.snValues, keys, values)

	def setValues(self, data, propPath, values):

		# path resolved
		if len(propPath) == 1:

			# if there is only one value, then it shouldn't be an array
			if len(values) == 1:
				values = values[0]

			data[propPath[0]] = values

		# resolve path
		else:
			# make a dictionary if we need one
			if propPath[0] not in data:
				data[propPath[0]] = {}

			if len(propPath) == 2 and propPath[1].isdigit() and type(data[propPath[0]]) is not dict:
				data[propPath[0]] = { 0: data[propPath[0]] }

			# go deeper
			self.setValues(data[propPath[0]], propPath[1:], values)
