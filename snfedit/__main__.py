import os
import sys
import pprint
import julian
import datetime

from .theme import *

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

try:
	from .MainWindowLayout import Ui_MainWindow
except ImportError:
	sys.exit("failed to import MainWindowLayout.\nDid you run the buildui script?")

from .models.CustomDirModel import CustomDirModel
from .models.CustomFileModel import CustomFileModel
from .snffile import SnfFile

class MainWindow(QMainWindow, Ui_MainWindow):

	selectedFiles = {}
	currentPreviewFile = None

	def __init__(self, *args, **kwargs):
		super(MainWindow, self).__init__(*args, **kwargs)
		self.setupUi(self)

		# setup selected file view

		self.selectedFilesListViewModel = QStandardItemModel()

		self.selectedFilesListView.setModel(self.selectedFilesListViewModel)

		self.selectedFilesListView.selectionModel().currentChanged.connect(
		    self.onFileItemPreviewSelectionChanged)

		# setup file dir view

		rootPath = os.path.join(QDir.currentPath(), 'samples')

		# try:
		# 	rootPath = parser.positionalArguments().pop(0)
		# except IndexError:
		# 	rootPath = None

		self.fileDirViewModel = CustomDirModel()

		# what file types are shown
		self.fileDirViewModel.setNameFilters(["*.snf"])
		self.fileDirViewModel.setNameFilterDisables(False)

		if parser.isSet(dontUseCustomDirectoryIconsOption):
			self.fileDirViewModel.iconProvider().setOption(
			    QFileIconProvider.DontUseCustomDirectoryIcons)

		self.fileDirView.setModel(self.fileDirViewModel)

		self.fileDirViewModel.setRootPath(QDir.currentPath())

		if rootPath is not None:
			rootIndex = self.fileDirViewModel.index(QDir.cleanPath(rootPath))
			if rootIndex.isValid():
				self.fileDirView.setRootIndex(rootIndex)

		self.fileDirView.setAnimated(True)
		self.fileDirView.setIndentation(14)
		self.fileDirView.setSortingEnabled(False)
		self.fileDirView.setAlternatingRowColors(True)
		self.fileDirView.setHeaderHidden(True);
		self.fileDirView.hideColumn(1)
		self.fileDirView.hideColumn(2)
		self.fileDirView.hideColumn(3)

		self.fileDirViewModel.checksChanged.connect(self.onFileItemSelectionChanged)
		self.fileDirCollapseAllButton.clicked.connect(
		    self.onFileDirCollapseAllButtonClicked)


		self.fileDataTreeViewModel = QStandardItemModel()
		self.fileDataTreeViewModel.setHorizontalHeaderLabels(['Property', 'Value'])
		self.fileDataTreeView.setModel(self.fileDataTreeViewModel)
		self.fileDataTreeView.setUniformRowHeights(True)
		self.fileDataTreeView.setAlternatingRowColors(True)

		self.dataCollapseAllButton.clicked.connect(
		    self.onFileDataCollapseAllButtonClicked)

		self.newChainButton.clicked.connect(self.onNewChainButtonClicked)
		self.editChainButton.clicked.connect(self.onEditChainButtonClicked)
		self.removeChainButton.clicked.connect(self.onRemoveChainButtonClicked)


		# self.fileDataTreeView.setIndentation(10)

	@pyqtSlot()
	def onNewChainButtonClicked(self):


	@pyqtSlot()
	def onEditChainButtonClicked(self):

	@pyqtSlot()
	def onRemoveChainButtonClicked(self):

	@pyqtSlot()
	def onFileDirCollapseAllButtonClicked(self):
		self.fileDirView.collapseAll()

	@pyqtSlot()
	def onFileDataCollapseAllButtonClicked(self):
		self.fileDataTreeView.collapseAll()

	@pyqtSlot(str, int)
	def onFileItemSelectionChanged(self, path, value):
		if path.endswith('.snf'):
			if value == 0:
				self.selectedFiles.pop(path)
			else:
				self.selectedFiles[path] = SnfFile(path)

			self.updateSelectedFilesList()

	@pyqtSlot(QModelIndex, QModelIndex)
	def onFileItemPreviewSelectionChanged(self, current, prev):
		self.setPreviewFile(
		    self.selectedFilesListViewModel.itemFromIndex(current).file)

	def updateSelectedFilesList(self):

		self.selectedFilesListViewModel.clear()

		for value in self.selectedFiles.values():

			# item = CustomFileModel(key, os.path.basename(key)[:-4])
			item = CustomFileModel(value)

			self.selectedFilesListViewModel.appendRow(item)

	def setPreviewFile(self, targetfile):

		self.currentPreviewFile = targetfile

		self.fileDataTreeViewModel.clear()
		self.fileDataTreeViewModel.setHorizontalHeaderLabels(['Property', 'Value'])

		self.loadTree(self.fileDataTreeViewModel, targetfile.data)

		self.fileDataTreeView.setFirstColumnSpanned(
		    0, self.fileDataTreeView.rootIndex(), True)

		self.fileDataTreeView.expandAll()
		self.fileDataTreeView.resizeColumnToContents(0)

	def loadTree(self, parent, data):

		for key, value in data.items():

			itemProperty = QStandardItem(str(key))

			# load sub tree
			if type(value) is dict:
				self.loadTree(itemProperty, value)
				parent.appendRow(itemProperty)

			# load property
			else:
				itemValue = QStandardItem(str(value))
				parent.appendRow([itemProperty, itemValue])


if __name__ == '__main__':
	app = QApplication(sys.argv)
	app.setApplicationName("test")

	QCoreApplication.setApplicationVersion(QT_VERSION_STR)

	# setup command args

	parser = QCommandLineParser()

	parser.addHelpOption()
	parser.addVersionOption()
	dontUseCustomDirectoryIconsOption = QCommandLineOption(
	    'c', "Set QFileIconProvider.DontUseCustomDirectoryIcons")
	parser.addOption(dontUseCustomDirectoryIconsOption)
	parser.addPositionalArgument('directory', "The directory to start in.")

	parser.process(app)

	window = MainWindow()

	# apply stylesheet
	# file = QFile(":/dark.qss")
	# file.open(QFile.ReadOnly | QFile.Text)
	# stream = QTextStream(file)
	# app.setStyleSheet(stream.readAll())

	# show window
	window.show()

	app.exec_()
