import os
from PyQt5.QtGui import QStandardItem
from PyQt5.QtCore import *

class CustomFileModel(QStandardItem):

	def __init__(self, file, parent=None):
		QStandardItem.__init__(self, parent)
		self.file = file
		self.setText(os.path.basename(self.file.path)[:-4])