from PyQt5.QtWidgets import QFileSystemModel
from PyQt5.QtCore import *

class CustomDirModel(QFileSystemModel):

	checks = {}
	checksChanged = pyqtSignal(str, int)

	def __init__(self, parent=None):
		QFileSystemModel.__init__(self, None)
		self.checks = {}

	def data(self, index, role=Qt.DisplayRole):
		if role == Qt.CheckStateRole and index.column() == 0:
			return self.checkState(index)
		return QFileSystemModel.data(self, index, role)

	def flags(self, index):
		if self.filePath(index).endswith('.snf'):
			return QFileSystemModel.flags(self, index) | Qt.ItemIsUserCheckable
		return QFileSystemModel.flags(self, index)

	def checkState(self, index):
		if self.filePath(index).endswith('.snf'):
			if index in self.checks:
				return self.checks[index]
			return Qt.Unchecked
		return None

	def setData(self, index, value, role):
		if (role == Qt.CheckStateRole and index.column() == 0):
			self.checks[index] = value
			self.checksChanged.emit(self.filePath(index), value)

			return True
		return QFileSystemModel.setData(self, index, value, role)
