from cx_Freeze import setup, Executable

setup(
	name = "SNFEdit",
	version = "0.1.0",
	options = {
		"build_exe": {
			'packages': ["snfedit"],
			'include_files': ["samples"],
			"include_msvcr": True
		}
	},
	executables = [Executable(script="snfedit.py", base="Win32GUI")]
)